/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.cameraview;

import android.view.View;

import java.util.Set;

abstract class CameraViewImpl {

    // 相机状态接口
    protected final Callback mCallback;

    // 预览接口
    protected final PreviewImpl mPreview;

    CameraViewImpl(Callback callback, PreviewImpl preview) {
        mCallback = callback;
        mPreview = preview;
    }

    View getView() {
        return mPreview.getView();
    }

    /**
     * @return {@code true} if the implementation was able to start the camera session.
     */
    abstract boolean start();

    abstract void stop();

    abstract boolean isCameraOpened();

    abstract void setFacing(int facing);

    abstract int getFacing();

    abstract Set<AspectRatio> getSupportedAspectRatios();

    /**
     * @return {@code true} if the aspect ratio was changed.
     */
    abstract boolean setAspectRatio(AspectRatio ratio);

    /**
     *
     * @return
     */
    abstract AspectRatio getAspectRatio();

    /**
     * 设置自动对焦
     * @param autoFocus
     */
    abstract void setAutoFocus(boolean autoFocus);

    /**
     * 获取自动对焦
     * @return
     */
    abstract boolean getAutoFocus();

    /**
     * 设置闪光灯的状态
     * @param flash
     */
    abstract void setFlash(int flash);

    /**
     *   获取闪光灯的状态
     */
    abstract int getFlash();

    /**
     * 拍照
     */
    abstract void takePicture();

    /**
     * 设置显示方向
     * @param displayOrientation
     */
    abstract void setDisplayOrientation(int displayOrientation);

    /**
     * 相机状态接口 开启、关闭、拍照
     */
    interface Callback {

        void onCameraOpened();

        void onCameraClosed();

        void onPictureTaken(byte[] data);

    }

}
